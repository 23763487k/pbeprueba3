# PBE prueba3

Creacion de CRUD, con FastAPI

-----------------------------------
Previamente instalar:
    pip install fastapi
	pip install pymongo

Y activamos con:
    uvicorn app:app --reload

Insertamos en los datos mediante un JSON de la siguiente forma:

{
    "marca": "Nexus",
    "modelo": "VGA-3000",
    "ram": 8,
    "hdd": 512
}
