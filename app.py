from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from connect import database


app = FastAPI()

# Pydantic model for Laptop
class Laptop(BaseModel):
    marca: str
    modelo: str
    ram: int        # in GB
    hdd: int        # in GB

# CREATE (Crear registro)
@app.put("/laptop/create")
async def create_lap(laptop: Laptop):
    db = database()
    lap = db.laptops
    lap.insert_one(laptop.dict())
    return {"message": "Laptop created"}

# READ (Leer registros)
@app.get("/laptop/all")
async def read_laps():
    db = database()
    lap = db.laptops
    laptops = lap.find({}).to_list()
    return laptops

# UPDATE (Actualizar registro)
@app.put("/laptop/update/{modelo}")
def update_lap(modelo: str, laptop_update: Laptop):
    db = database()
    lap = db.laptops
    lap.update_one({"modelo": modelo}, {"$set": laptop_update.dict()})
    return {"message": "Laptop updated"}

# DELETE (Eliminar registro)
@app.delete("/laptop/delete/{modelo}")
def delete_lap(modelo: str):
    db = database()
    lap = db.laptops
    lap.delete_one({"modelo": modelo})
    return {"message": "Laptop deleted"}
